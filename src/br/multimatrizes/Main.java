package br.multimatrizes;

public class Main {
	public static void main(String args[]) {
		int i=0;
		Paralelo paralelo = new Paralelo();
		Paralelov4 paralelov4 = new Paralelov4();
		Paralelov8 paralelov8 = new Paralelov8();
		Sequencial sequencial = new Sequencial();
		for (i=100; i<=2000; i+=100) {
			System.out.println("\n ----- Executando a multiplicação de matrizes sequencial. Base da matrícula: " + i);
			sequencial.sequencial(i);
		}
		
		for (i=100; i<=2000; i+=100) {
			
			System.out.println("\n ----- Executando a multiplicação de matrizes com 2 Threads. Base da matrícula: " + i);
			paralelo.paralelo(i);
			
		}
		
		for (i=100; i<=2000; i+=100) {
			System.out.println("\n ----- Executando a multiplicação de matrizes com 4 Threads. Base da matrícula: " + i);
			paralelov4.paralelov4(i);
		}
		
		for (i=100; i<=2000; i+=100) {
			System.out.println("\n ----- Executando a multiplicação de matrizes com 8 Threads. Base da matrícula: " + i);
			paralelov8.paralelov8(i);
		}
			
	}
}
