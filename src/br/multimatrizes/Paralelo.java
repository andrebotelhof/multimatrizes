package br.multimatrizes;

/**
  Programa em Java correspondente � vers�o sequencial da multiplica��o de matrizes.
  @autor Roland Teodorowitsch
  @version 4 set. 2018
 */

public class Paralelo {

  // DIMENSOES DAS MATRIZES
  public static int SIZE = 0;
  // ESTRUTURAS DE DADOS COMPARTILHADA
  public static int[][] m1;
  public static int[][] m2;
  public static int[][] mres;

  
  public void paralelo(int quantidade) {
	SIZE = quantidade;
    // INICIALIZA OS ARRAYS A SEREM MULTIPLICADOS
    m1 = new int[SIZE][SIZE];
    m2 = new int[SIZE][SIZE];
    mres = new int[SIZE][SIZE];
    if (m1[0].length != m2.length || mres.length != m1.length || mres[0].length != m2[0].length) {
       System.err.println("Impossivel multiplicar matrizes: parametros invalidos.");
       System.exit(1);
    }
    int k=1;
    for (int i=0 ; i<SIZE; i++) {
        for (int j=0 ; j<SIZE; j++) {
            if (k%2==0)
               m1[i][j] = -k;
            else
               m1[i][j] = k;
        }
        k++;
    }
    k=1;
    for (int j=0 ; j<SIZE; j++) {
        for (int i=0 ; i<SIZE; i++) {
            if (k%2==0)
               m2[i][j] = -k;
            else
               m2[i][j] = k;
        }
        k++;
    }

    // PREPARA PARA MEDIR TEMPO
    long inicio = System.nanoTime();

    //INICIALIZA AS THREADS
    Thread a1 = new Thread(t1);
    Thread a2 = new Thread(t2);
    
  
    	a1.start();
    	a2.start();
    	
	
	//AGUARDA FINALIZA��O DE AMBAS THREADS
    try {
        a1.join();
        a2.join();  
    } catch (InterruptedException ex) {
        ex.printStackTrace();
    }
    
    
 // OBTEM O TEMPO
    long fim = System.nanoTime();
    double total = (fim-inicio)/1000000000.0;

    // VERIFICA SE O RESULTADO DA MULTIPLICACAO ESTA CORRETO
    for (int i=0 ; i<SIZE; i++) {
        k = SIZE*(i+1);
        for (int j=0 ; j<SIZE; j++) {
            int k_col = k*(j+1);
            if (i % 2 ==0) {
               if (j % 2 == 0) {
                  if (mres[i][j]!=k_col)
                     //System.exit(1);
                	  System.out.println("Falha na 1 verifica��o");
               }
               else {
                  if (mres[i][j]!=-k_col)
                     //System.exit(1);
                	  System.out.println("Falha na 2 verifica��o");
               }
            }
            else {
               if (j % 2 == 0) {
                  if (mres[i][j]!=-k_col)
                     //System.exit(1);
                	  System.out.println("Falha na 3 verifica��o");
               }
               else {
                  if (mres[i][j]!=k_col)
                    // System.exit(1);
                	  System.out.println("Falha na 4 verifica��o");
               }
            }
        } 
    }

    // MOSTRA O TEMPO DE EXECUCAO
    System.out.printf(" O resultado da execu��o foi de: %f segundos.", total);
   
  }
  
  private static Thread t1 = new Thread() {
      public void run() {
          try{
        	    for (int i=0 ; i<mres.length; i=i+2) {
        	        for (int j=0 ; j<mres[0].length; j++) {
        	            mres[i][j] = 0;
        	            for (int k=0 ; k<m2.length; k++) {
        	                mres[i][j] += m1[i][k] * m2[k][j];
        	            }
        	        }
        	    }
        	    
          } catch (Exception e){}

      }
  };

  private static Thread t2 = new Thread() {
      public void run() {
          try{
        	  for (int i=1 ; i<mres.length; i=i+2) {
      	        for (int j=0 ; j<mres[0].length; j++) {
      	            mres[i][j] = 0;
      	            for (int k=0 ; k<m2.length; k++) {
      	                mres[i][j] += m1[i][k] * m2[k][j];
      	            }
      	        }
              }
          } catch (Exception e){}
     }
  };

}